import { LOCATION_ALLOWED, LOCATION_DENIED } from '../Types';

export function locationAllowed(coords) {
    return {
        type: LOCATION_ALLOWED,
        isAllowed: true,
        payload: coords
    };
}

export function locationDenied(error) {
    return {
        type: LOCATION_DENIED,
        isAllowed: false
    }
}