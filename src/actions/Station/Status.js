import { 
    STN_STATUS_GET_ERR,
    STN_STATUS_GET_OK, 
    STN_STATUS_SET_LAST_UPDATED } from '../Types';
    
import StationApi from '../../api/Station';

export function getStationStatusErr(error) {
    return {
        type: STN_STATUS_GET_ERR,
        payload: error
    }
}

export function getStationStatusOk(stationStatus, stationId) {
    return {
        type: STN_STATUS_GET_OK,
        stationId,
        payload: stationStatus
    }
}

export function setLastUpdated(minutes) {
    return {
        type: STN_STATUS_SET_LAST_UPDATED,
        payload: minutes
    }
}

// Note: Since no documentation available => In real life, this method will return single status.
// For now list is filtered in the FakeFilter Middleware.
export function getStationStatus(stationId) {
    return (dispatch) => {
        return StationApi.getStationStatus()
            .then((response) => {
                dispatch(getStationStatusOk(response.data, stationId));
            })
            .catch((error) => {
                dispatch(getStationStatusErr(error));
            });
    }
}