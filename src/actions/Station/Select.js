import { STN_SELECT } from '../../actions/Types';

export function selectStation(station) {
   return {
        type: STN_SELECT,
        payload: station
    }
}