import { 
    STN_LIST_LOADING,
    STN_LIST_CHANGE_FILTERMODE,
    STN_LIST_GETBY_LOC_ERR,
    STN_LIST_GETBY_LOC_OK,
    STN_LIST_GETBY_CAPACITY_ERR,
    STN_LIST_GETBY_CAPACITY_OK } from '../Types';
import StationApi from '../../api/Station';

export function loading() {
    return {
        type: STN_LIST_LOADING
    };
}

export function changeFilterMode(filterMode) {
    return {
        type: STN_LIST_CHANGE_FILTERMODE,
        payload: filterMode
    };
}

export function getStationListByLocErr(error) {
    return {
        type: STN_LIST_GETBY_LOC_ERR,
        payload: error
    }
}

export function getStationListByLocOk(stationList, coords) {
    return {
        type: STN_LIST_GETBY_LOC_OK,
        coords,
        payload: stationList
    }
}

// Note: Since no documentation available => In real life, coords will be passed in the api call.
// For now list is filtered in the FakeFilter Middleware.
export function getStationListByLoc(coords) {
    return (dispatch) => {
        dispatch(loading());
        return StationApi.getStationList()
            .then((response) => {
                dispatch(getStationListByLocOk(response.data, coords));
            })
            .catch((error) => {
                dispatch(getStationListByLocErr(error));
            });
    }
}

export function getStationListByCapacityErr(error) {
    return {
        type: STN_LIST_GETBY_CAPACITY_ERR,
        payload: error
    }
}

export function getStationListByCapacityOk(stationList) {
    return {
        type: STN_LIST_GETBY_CAPACITY_OK,
        payload: stationList
    }
}

export function getStationListByCapacity() {
    return (dispatch) => {
        dispatch(loading());
        return StationApi.getStationList()
            .then((response) => {
                dispatch(getStationListByCapacityOk(response.data));
            })
            .catch((error) => {
                dispatch(getStationListByCapacityErr(error));
            });
    }
}