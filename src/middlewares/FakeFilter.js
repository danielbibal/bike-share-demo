import { 
    STN_LIST_GETBY_CAPACITY_OK, 
    STN_LIST_GETBY_LOC_OK, 
    STN_STATUS_GET_OK } from '../actions/Types';
import * as _ from 'lodash';

export default function({ dispatch }) {

    const recordLimit = 10;

    function filterStatus(action) {
        const stationId = action.stationId;
        var data = action.payload.data.stations;
        data = data.filter(station => station.station_id === stationId);
        action.payload.data = (data && data.length >=1) ? data[0]:{};
        return action;
    }

    function filterByCapacity(action) {
        var data = action.payload.data.stations;
        const limit = data.length >= recordLimit ? recordLimit: data.length;

        data = _.orderBy(data, ['capacity'], ['desc'])
        data = _.take(data, limit);
        action.payload.data = data;
        
        return action;
    }

    function calculateDistance(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit==="K") { dist = dist * 1.609344 }
        if (unit==="N") { dist = dist * 0.8684 }
        return dist
    }

    function filterByLocation(action) {
        var data = action.payload.data.stations;
        const limit = data.length >= recordLimit ? recordLimit: data.length;
        
        let { lat, lon } = action.coords;
        // in any case it's empty, default to toronto coordinates.
        lat = lat ? lat: 43.6532;
        lon = lon ? lon: 79.3832;
        
        data.forEach(function(station){
            station.distance = calculateDistance(lat, lon, station.lat, station.lon,'K');
        });
        data = _.orderBy(data, ['distance'], ['asc'])
        data = _.take(data, limit);
        action.payload.data = data;
        return action;
    }

    return next => action => {
        switch(action.type) {
            case STN_LIST_GETBY_CAPACITY_OK:
                action = filterByCapacity(action);
                next(action);
                break;
            case STN_LIST_GETBY_LOC_OK:
                action = filterByLocation(action);
                next(action);
                break;
            case STN_STATUS_GET_OK:
                action = filterStatus(action);
                next(action);
                break;
            default:
                next(action);
        }
            
    };
  }
  