import axios from 'axios';
const ROOT_URL =  'https://tor.publicbikesystem.net/ube/gbfs/v1/en/'; //process.env.REACT_APP_API_ROOT_URL; - env file not supported in S3, use static for now.

const getStationList = () => {
    const url = `${ROOT_URL}station_information`;
    return axios.get(url);
}

const getStationStatus = () => {
    const url = `${ROOT_URL}station_status`;
    return axios.get(url);
}

export default { getStationList, getStationStatus };