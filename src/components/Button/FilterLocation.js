import React from 'react'
import { connect } from 'react-redux';
import { changeFilterMode } from '../../actions/Station/List';
import { CONST_FILTER_LOCATION } from '../../common/Constants'
const FilterLocationButton = props => {

    function filterLocation() {
        const { changeFilterMode } = props;
        changeFilterMode(CONST_FILTER_LOCATION);
    }
    
    return(
        <button className="btn btn-success" onClick={filterLocation}>
            <i className="fas fa-map-marker-alt"></i>
            <span>filter location</span>
        </button>
    );
}

const mapDispatchToProps = { changeFilterMode };

export default connect(null, mapDispatchToProps) (FilterLocationButton);