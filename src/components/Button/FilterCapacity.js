import React from 'react'
import { connect } from 'react-redux';
import { changeFilterMode } from '../../actions/Station/List';
import { CONST_FILTER_CAPACITY } from '../../common/Constants';

const FilterCapacityButton = props => {

    function filterCapacity() {
        const { changeFilterMode } = props;
        changeFilterMode(CONST_FILTER_CAPACITY);
    }
  
    return(
        <button className="btn btn-success" onClick={filterCapacity}>
            <i className="fas fa-sort-amount-down"></i>
            <span>filter capacity</span>
        </button>
    );
}

const mapDispatchToProps = { changeFilterMode };

export default connect(null, mapDispatchToProps) (FilterCapacityButton);