import React from 'react';
import StationListItem from './ListItem';
import Loading from '../../Loading/Loading';

const StationList = (props) => {

    const renderListItems = () => {
    
        const stationList = props.stationList ? props.stationList:{};
        const stations = stationList.stations ? stationList.stations.data:[];
        
        if(!stations && stations.length === 0){ return;}
        
        const list = [];
        stations.forEach(function(station){
            list.push(
                <li key={station.station_id}>
                    <StationListItem station={station} history={props.history} />
                </li>
            );
        });
        return list;
    }

    const renderList = () => {
        const stationItems = renderListItems();
        return (
            <ul className="stn-list">
                { stationItems }
            </ul>
        );     
    }
    
    const stationList = (props.stationList && props.stationList.isLoading) ? <Loading /> : renderList();
    return(
        <div className="stn-content-container">
            {stationList}
        </div>
    );
}

export default StationList;