import React from 'react';
import FilterCapacityButton from '../../Button/FilterCapacity';
import FilterLocationButton from '../../Button/FilterLocation';
import { CONST_FILTER_LOCATION} from '../../../common/Constants';

const StationListHeader = (props) => {

    const filterBtn = (!props.filterMode || props.filterMode === CONST_FILTER_LOCATION) ? <FilterCapacityButton />: <FilterLocationButton location={props.location} />
    return(
        <div className="stn-header-container">
            <div className="stn-header">
                <div className="stn-header-inner">
                    <h2><i className="fas fa-warehouse"></i> Bike Stations</h2>
                    {filterBtn}
                </div>
            </div>
        </div>
    );
}

export default StationListHeader;