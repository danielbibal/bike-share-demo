import React from 'react';
import { connect } from 'react-redux';
import { selectStation } from '../../../actions/Station/Select';
import { getLabel, getCountCSS } from '../../../reducers/Index';

const StationListItem = props => {

    const station = props.station;
    const label = getLabel(station.capacity, 'Bike');

    const selectStation = () => {
        const url = '/station-status/' + station.station_id;
        const { history, selectStation } = props;
        selectStation(station);
        history.push(url);
    }
    
    const capacityCSS = getCountCSS(station.capacity);
    return (
        <div className="stn-list-item" onClick={selectStation}>
            <span>
                <h4>{station.name}</h4>
                <span>Capacity: <span className={capacityCSS}>{station.capacity}</span> {label} </span>
            </span>
            <i className="fas fa-chevron-right"></i>
        </div>
    );
}

const mapDispatchToProps = { selectStation };

export default connect(null, mapDispatchToProps)(StationListItem);