import React from 'react';
import { getLabel, getCountCSS } from '../../../reducers/Index';

const StationStatus = (props) => {

    const { stationName } = props;
    const status = props.stationStatus ? props.stationStatus:{};
    const bikeCount = status.station ? status.station.data.num_bikes_available: null,
          dockCount = status.station ? status.station.data.num_docks_available: null;
    const bikeLabel = getLabel(bikeCount, 'Bike'),
          dockLabel = getLabel(dockCount, 'Dock');
    
    const bikeCountCSS = getCountCSS(bikeCount),
          dockCountCSS = getCountCSS(dockCount);
    
    var mins = props.stationStatus.lastUpdatedMinutes;
    mins = (mins !== undefined) ? mins: 0;
    const updateMessage = 'Updated ' + mins  + ' ' + getLabel(mins, 'minute') + ' ago';
    
    return(
        <div className="stn-content-container">
            <div className="stn-status-container">
                <h1>{ stationName }</h1>
                <div className="stn-status-box">
                    <div className="status-info">
                        <span className={bikeCountCSS}>{bikeCount}</span>
                        {bikeLabel}
                    </div>
                    <div className="status-info">
                        <span className={dockCountCSS}>{dockCount}</span>
                        {dockLabel}
                    </div>
                </div>
                <div className="stn-status-footer">
                    <span>{ updateMessage }</span>
                </div>
            </div>
        </div>
    );
}

export default StationStatus;