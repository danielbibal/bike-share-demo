import React from 'react';
import { NavLink } from 'react-router-dom';

const StationStatusHeader = () => {

    return(
        <div className="stn-header-container">
            <div className="stn-header">
                <div className="stn-header-inner">
                    <h2><NavLink to="/"><i className="fas fa-chevron-left"></i></NavLink> Bike Stations</h2>
                </div>
            </div>
        </div>
    );
}

export default StationStatusHeader;