import React, { Component } from 'react';
import StationApp from '../screens/Station/App';
import './App.css';
class App extends Component {  
  render() {
    return <StationApp />
  }
}

export default App;
