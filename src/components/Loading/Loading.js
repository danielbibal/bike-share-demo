import React from 'react';

export const Loader = () => {

    return(
        <div className="loading-container">
            <div className="spinner">
                <p>Loading</p>
                <div className="bounce1"></div>
                <div className="bounce2"></div>
                <div className="bounce3"></div>
            </div>
        </div>
    );
}

export default Loader;