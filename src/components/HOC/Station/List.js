import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getStationListByLoc, getStationListByCapacity } from '../../../actions/Station/List';
import { CONST_FILTER_LOCATION, CONST_FILTER_CAPACITY } from '../../../common/Constants';

export default function (WrappedComponent) {

    class WithStationList extends Component {

        componentDidMount() {
            this.loadData();
        }

        loadData = () => {
            const { stationList, location, getStationListByLoc, getStationListByCapacity } = this.props;

            switch(stationList.filterMode) {
                case CONST_FILTER_LOCATION:
                    if(location.isAllowed && !stationList.isLoading && stationList.fetchData) {
                        getStationListByLoc(location.coords);
                    }
                    break;
                case CONST_FILTER_CAPACITY:
                    if(!stationList.isLoading && stationList.fetchData) {
                        getStationListByCapacity();
                    }
                    break;
                default:
                    return;
            }
        };

        render() {
            const { stationList, location, history } = this.props;
            return <WrappedComponent {...{stationList}} {...{location}} {...{history}} />
        }
    }

    const mapStateToProps = (state) => {
        const { stationList, location } = state;
        return {
            location,
            stationList
        }
    }

    const mapDispatchToProps = { getStationListByLoc, getStationListByCapacity };

    return connect(mapStateToProps, mapDispatchToProps)(WithStationList);
}