import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getStationStatus, setLastUpdated } from '../../../actions/Station/Status';
import { getTimeElapsed } from '../../../reducers/Index';

export default function (WrappedComponent) {

    class WithStationStatus extends Component {

        componentDidMount() {
            this.loadData();
            this.updateElapsedTimer = setInterval(this.updateElapsed, 30000);
        }
        componentWillUnmount() {
            clearInterval(this.updateElapsedTimer);
        }

        updateElapsed = () => {
            const { stationStatus } = this.props;
            if(stationStatus.station) {
                var elapsed = getTimeElapsed(stationStatus.station.last_updated);
                const { setLastUpdated } = this.props;
                setLastUpdated(elapsed);
            }
        }

        loadData = () => {
            const { getStationStatus, match } = this.props;
            const stationId = match.params.stationId;
            getStationStatus(stationId);
        };

        render() {
            const { stationStatus, stationSelected } = this.props;
            return <WrappedComponent {...{stationStatus}} {...{stationSelected}} />
        }
    }

    const mapStateToProps = (state) => {
        const { stationStatus, stationSelected } = state;
        return {
            stationStatus,
            stationSelected
        }
    }

    const mapDispatchToProps = { getStationStatus, setLastUpdated };

    return connect(mapStateToProps, mapDispatchToProps)(WithStationStatus);
}