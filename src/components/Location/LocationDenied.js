import React from 'react';

const LocationDenied = (props) => {
    return(
        <div className="location-denied-alert">
            <span>
                Location sharing was denied. Please allow location sharing to view bike station near you.
            </span>
        </div>
    );
}

export default LocationDenied;