import { combineReducers } from 'redux';
import StationListReducer from './Station/List';
import StationSelectReducer from './Station/Select';
import StationStatusReducer from './Station/Status';
import LocationReducer from './Location';

const rootReducer = combineReducers({
    stationList: StationListReducer,
    stationSelected: StationSelectReducer,
    stationStatus: StationStatusReducer,
    location: LocationReducer
});

export function getLabel(count, context) {
    if(count === '' || count === undefined || count === null) return;
    return count > 1 ? context + 's' : context;
}

export function getCountCSS(count) {
    if(count === '' || count === undefined || count === null) return;
    if(count === 0 ) return 'color-danger';
    if(count === 1 ) return 'color-warning';
    if(count > 1) return 'color-success';
}

export function getTimeElapsed(timestamp) {
    var date = timestamp*1000;
    var now = + new Date();
    var diff = now - date;
    var minutes = Math.floor(diff/1000/60);
    return minutes;
}

export default rootReducer;