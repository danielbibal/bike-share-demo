import { 
    STN_LIST_LOADING,
    STN_LIST_CHANGE_FILTERMODE,
    STN_LIST_GETBY_LOC_ERR,
    STN_LIST_GETBY_LOC_OK,
    STN_LIST_GETBY_CAPACITY_ERR,
    STN_LIST_GETBY_CAPACITY_OK } from '../../actions/Types';

const StationListReducer = (state = {}, action) => {

    switch(action.type) {
        case STN_LIST_LOADING:
            return { ...state, ...{isLoading:true, fetchData: false} };
        case STN_LIST_CHANGE_FILTERMODE:
            return { ...state, ...{ filterMode:action.payload, fetchData: true, stations: null } };
        case STN_LIST_GETBY_LOC_OK:
            return { ...state, ...{ stations: action.payload, error: null, haveError: false, isLoading: false, fetchData: false } };
        case STN_LIST_GETBY_CAPACITY_OK:
            return { ...state, ...{ stations: action.payload, error: null, haveError: false, isLoading: false, fetchData: false } };
        case STN_LIST_GETBY_LOC_ERR:
            return { ...state, ...{ stations: null, error: action.payload, haveError: true, isLoading: false, fetchData: false } };
        case STN_LIST_GETBY_CAPACITY_ERR:
            return { ...state, ...{ stations: null, error: action.payload, haveError: true, isLoading: false, fetchData: false } };
        default:
            return state;
    }
}

export default StationListReducer;