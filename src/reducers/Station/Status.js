import { STN_STATUS_GET_ERR, STN_STATUS_GET_OK, STN_STATUS_SET_LAST_UPDATED } from '../../actions/Types';

const StationStatusReducer = (state = {}, action) => {

    switch(action.type) {
        case STN_STATUS_GET_ERR:
            return { ...state, ...{ station: null, error: action.payload, haveError: false } };
        case STN_STATUS_GET_OK:
            return { ...state, ...{ station: action.payload, error: null, haveError: false, lastUpdatedMinutes:0 } };
        case STN_STATUS_SET_LAST_UPDATED:
            return { ...state, ...{ lastUpdatedMinutes: action.payload} };
        default:
            return state;
    }
}

export default StationStatusReducer;