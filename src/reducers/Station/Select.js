import { STN_SELECT } from '../../actions/Types';

const StationSelectReducer = (state = {}, action) => {

    switch(action.type) {
        case STN_SELECT:
            return action.payload
        default:
            return state;
    }
}

export default StationSelectReducer;

