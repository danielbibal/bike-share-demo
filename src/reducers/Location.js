import { LOCATION_ALLOWED, LOCATION_DENIED } from '../actions/Types';

const LocationReducer = (state = {}, action) => {
    switch(action.type) {
        case LOCATION_ALLOWED:
            return { ...state, ...{ isAllowed:true, coords: action.payload} };
        case LOCATION_DENIED:
            return { ...state, ...{ isAllowed: false, coords: null} };
        default:
            return state;
    }
}

export default LocationReducer;