import React from 'react';
import StationStatusHeader from '../../components/Station/Status/Header';
import StationStatus from '../../components/Station/Status/Status';

const StationStatusUI = (props) => {
    
    const stationName = props.stationSelected ? props.stationSelected.name:'';
    const stationStatus = props.stationStatus ? props.stationStatus:{};
    return(
        <div>
            <StationStatusHeader />
            <StationStatus stationName={stationName} stationStatus={stationStatus} />
        </div>
    );
}

export default StationStatusUI;