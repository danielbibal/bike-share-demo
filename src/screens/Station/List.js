import React from 'react';
import StationListHeader from '../../components/Station/List/Header';
import StationList from '../../components/Station/List/List';
import LocationDenied from '../../components/Location/LocationDenied';
import { CONST_FILTER_LOCATION } from '../../common/Constants';

const StationListUI = (props) => {

    const location = props.location ? props.location:{};
    const stationList = props.stationList ? props.stationList:{};
    const { history } = props;

    let detail;
    if(location.isAllowed === false && stationList.filterMode === CONST_FILTER_LOCATION){
        detail = <LocationDenied />
    }else {
        detail = <StationList stationList={stationList} {...{history}} />
    }

    return(
        <div>
            <StationListHeader filterMode = {stationList.filterMode} location={location} />
            { detail }        
        </div>
    );
}

export default StationListUI;