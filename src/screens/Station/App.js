import React, { Component } from 'react';
import { connect } from 'react-redux'; 
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { locationAllowed, locationDenied  } from '../../actions/Location/Location';
import { changeFilterMode } from '../../actions/Station/List';
import WithStationList from '../../components/HOC/Station/List';
import WithStationStatus from '../../components/HOC/Station/Status';
import { CONST_FILTER_LOCATION } from '../../common/Constants';

import StationListUI from './List';
import StationStatusUI from './Status';

class StationApp extends Component {

    componentDidMount() {
        this.requestLocaton(); 
        this.checkFilterMode();
    }
    
    checkFilterMode() {
        const { stationList, changeFilterMode } = this.props;
        if (!stationList || !stationList.filterMode) {
            changeFilterMode(CONST_FILTER_LOCATION);
        }
    }
    success = (position) => {
        const coords = {
            lat: position.coords.latitude,
            lon: position.coords.longitude
        }
        const { locationAllowed } = this.props;
        locationAllowed(coords);
    }
    
    error = (err) => {
        const { locationDenied } = this.props;
        locationDenied(err);
    }

    requestLocaton() {
        navigator.geolocation.getCurrentPosition(this.success, this.error);
    }

    render() {
        return(
            <BrowserRouter>
                <Switch>
                    <Route path="/" component={ WithStationList(StationListUI) } exact />
                    <Route path="/station-status/:stationId?" component={ WithStationStatus(StationStatusUI) } />
                    <Route component={ WithStationList(StationListUI) } />
                </Switch>
            </BrowserRouter>
        );
    }

}

const mapStateToProps = (state) => {
    const { location, stationList } = state;
    return {
        location,
        stationList
    }
}

const mapDispatchToProps = { locationAllowed, locationDenied, changeFilterMode };

export default connect(mapStateToProps, mapDispatchToProps)(StationApp);