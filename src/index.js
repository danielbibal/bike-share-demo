import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import App from "./components/App";
import './polyfill';

import fakeFilter from './middlewares/FakeFilter';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers/Index";
import thunkMiddleware from 'redux-thunk';
import ES6Promise from  'es6-promise';
import dotenv from "dotenv";
ES6Promise.polyfill();
dotenv.config();

const store = createStore(
  reducers,
  applyMiddleware(thunkMiddleware, fakeFilter)
);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
